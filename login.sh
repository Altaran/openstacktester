clear
echo -e "\e[35m ###WELCOME IN LOGIN MODULE### \e[0m"

source $PWD/user.conf
source $PWD/keystonerc_admin
#Create Project, User and Project-User Role if it doesnt exist
keystone tenant-create       \
         --name $project_name

keystone user-create         \
         --name $user_name    \
         --pass $user_pwd       \
         --tenant $project_name

#keystone user-role-add       \
#         --user $user_name    \
#         --role _member_     \
#         --tenant $project_name

# Create RC file for this user
cat >> keystonerc_$user_name<<EOF
export OS_USERNAME=$user_name
export OS_TENANT_NAME=$project_name
export OS_PASSWORD=$user_pwd
export OS_AUTH_URL="http://localhost:5000/v2.0/"
export PS1='[\u@\h \W(keystone_$username)]\$ '
EOF

chmod -w keystonerc_$user_name
source $PWD/keystonerc_$user_name
source $PWD/security_rules.conf

echo ''
