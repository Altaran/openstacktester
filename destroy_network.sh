clear
echo -e "\e[35m ###WELCOME IN DESTROY NETWORK MODULE###"
echo ''

source $PWD/user.conf
source $PWD/keystonerc_$user_name
source $PWD/network.conf

echo ''
echo -e "\e[31m Deleting Instances: \e[0m"

#Deleting VMs
for i in "${machine_name[@]}"
do	
	VM_ID=$(nova list | grep $i | awk '{print $2;}')
	F_IP=$(nova floating-ip-list | grep $VM_ID | awk '{ print $2;}')
	nova floating-ip-delete $F_IP
        nova delete $VM_ID
done

echo ''
echo -e "\e[31m Deleting Subnets: \e[0m"

ROUTER_ID=$(neutron router-list | grep $router_name | awk '{print $2;}')
for i in "${subnet_name[@]}"
do
        SUBNET_ID=$(neutron subnet-list | grep $i | awk '{print $2;}')

        neutron router-interface-delete $ROUTER_ID $SUBNET_ID

        neutron subnet-delete \
                $SUBNET_ID
done				
	
echo ''	
echo -e "\e[31m Deleting Router: \e[0m"
		
neutron router-gateway-clear \
        $ROUTER_ID

neutron router-delete \
        $ROUTER_ID

echo ''
echo -e "\e[31m Deleting Networks: \e[0m"

#Deleting networks
for i in "${net_name[@]}"
do
	NET_ID=$(neutron net-list | grep $i | awk '{print $2;}')
        neutron net-delete $NET_ID
done

echo ''
echo -e "\e[31m Clearing Namespaces: \e[0m"

/usr/local/bin/neutron-netns-cleanup --config-file=/etc/neutron/neutron.conf --config-file=/etc/neutron/l3_agent.ini
