clear

echo -e "\e[35m ###WELCOME IN CREATING NETWORK MODULE###"
echo ''

source $PWD/user.conf
source $PWD/keystonerc_$user_name
source $PWD/VM.conf
source $PWD/network.conf

echo ''
echo -e "\e[31m Creating Networks with subnets: \e[0m"

# Create networks with subnets
n=0
for i in "${net_name[@]}"
do
        neutron net-create $i
        neutron subnet-create $i ${subnet_IP[n]}/24 --name ${subnet_name[n]}
		n=$n+1
done

echo ''
echo -e "\e[31m Creating router with external connection: \e[0m"

# Creating Router with external network
neutron router-create $router_name

ROUTER_ID=$(neutron router-list | grep $router_name | awk '{print $2;}')
EXT_NET=$(neutron net-list | grep public | awk '{print $2;}')

neutron router-gateway-set \
        $ROUTER_ID $EXT_NET

echo ''
echo -e "\e[31m Adding interfaces: \e[0m"
# Ading interfaces to router
for i in "${subnet_name[@]}"
do
        SUBNET_ID=$(neutron subnet-list | grep $i | awk '{print $2;}')

        neutron router-interface-add $ROUTER_ID $SUBNET_ID
done

echo ''
echo -e "\e[31m Creating instances: \e[0m"
# Creating instances
m=0
n=0
for i in "${net_name[@]}"
do
	NET_ID=$(neutron net-list | grep $i | awk '{print $2;}')
	for (( k=0 ; k<${machine_count[m]} ; ++k ))
	do
		nova boot ${machine_name[n]} --image ${machine_type[n]} --flavor ${machine_flavor[n]} --nic net-id=$NET_ID --security-groups default		
		n=$n+1
	done
	m=$m+1
done

echo ''
echo -e "\e[31m Creating floating IPs for instances: \e[0m"
#Adding floating IPs
for i in "${machine_name[@]}"
do
	VM_ID=$( nova list | grep $i | awk '{print $2;}')
	FL_IP=$( nova floating-ip-create | grep -v Ip | awk '{ print $2;}')
	nova add-floating-ip $VM_ID $FL_IP
done
