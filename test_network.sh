clear
echo ''
echo -e "\e[35m #WELCOME IN NETWORK TESTING MODULE#  "
echo ''

#Include data needed for tests
source $PWD/user.conf
source $PWD/keystonerc_$user_name
source $PWD/network.conf

#Data fot router testing
ROUTER_ID=$(neutron router-list | grep $router_name | awk '{print $2;}')
ROUTER_IP=$(neutron router-list | grep $router_name | awk '{print $14;}' | cut -d '"' -f2)

echo -e "\e[31m Testing router external connection: \e[0m"
result=$(sudo ip netns exec qrouter-$ROUTER_ID ping -c 5 -q 8.8.8.8 )
echo $result
result=$(sudo ip netns exec qrouter-$ROUTER_ID ping -c 5 -q google.com)
echo $result
echo ''

for i in "${net_name[@]}"
do	
	#Data for networking tests
	NET_ID=$(neutron net-list | grep $i | awk '{print $2;}')
	#SUBNET_ID=$(neutron subnet-list | grep $i | awk '{print $2;}')
	#SUBNET_IP=$(neutron subnet-show $SUBNET_ID | grep gateway_ip  | awk '{print $4;}')	
	echo -e "\e[35m Tests for network $i"
	echo ''
	echo -e "\e[31m Testing router connection with network\e[0m" 
	result=$(sudo ip netns exec qdhcp-$NET_ID ping -c 5 -q $ROUTER_IP)
	echo $result
	echo -e "\e[31m Testing network connection with internet  \e[0m" 
	result=$(sudo ip netns exec qdhcp-$NET_ID ping -c 5 -q 8.8.8.8)
	echo $result		
	result=$(sudo ip netns exec qdhcp-$NET_ID ping -c 5 -q google.com)
	echo $result		
	
	echo -e "\e[31m Testing network connection with Instances \e[0m"
	for j in "${machine_name[@]}"
	do
		#Data for instance tests
		VM_IP=$(nova list | grep $j  | awk '{print $12;}' | cut -d '=' -f2 )
		
		result=$(sudo ip netns exec qdhcp-$NET_ID ping -c 5 -q $VM_IP)
		echo $result
	done
	echo ''
done
