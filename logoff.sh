clear
echo -e "\e[35m ###WELCOME IN LOGOFF MODULE### \e[0m"
source $PWD/user.conf
source $PWD/keystonerc_admin

USER_ID=$(openstack user list | grep $user_name | awk '{print $2;}')
PROJECT_ID=$(openstack project list | grep $project_name | awk '{print $2;}')
keystone tenant-delete $PROJECT_ID
keystone user-delete $USER_ID

chmod +w keystonerc_$user_name
rm keystonerc_$user_name
