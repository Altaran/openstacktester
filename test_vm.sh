
source $PWD/user.conf
source $PWD/keystonerc_$user_name
source $PWD/network.conf

for i in "${machine_name[@]}"
do
	VM_ID=$( nova list | grep $i | awk '{ print $2;}')
	tmp=$( nova show $VM_ID | grep vm_state | awk '{ print $4;}')
	while [ "$tmp" != "active" ]
	do	
		tmp=$( nova show $VM_ID | grep vm_state | awk '{ print $4;}')
	done
	echo -e  "\e[32m $i Active! \e[0m"
done
