clear
flags=(1 0 0 0) 

while [ ${flags[0]} -eq 1 ]
do
	echo -e "\e[35m ### WELCOME TO MOTHERBOARD ###\e[0m"
	echo ''

	echo -e "\e[94m CREATE && LOGIN : 1\e[0m"
	echo -e "\e[94m	CREATE_NETWORK : 2\e[0m"
	echo -e "\e[94m DESTROY_NETWORK : 3\e[0m"
	echo -e "\e[94m LOGOFF && DESTROY USER :4\e[0m"
	echo -e "\e[94m TEST NETWORK : 5\e[0m"
	echo -e "\e[94m EXIT : 6\e[0m"	

	echo ''
	echo -e "\e[94m Set your signal: \e[0m"
	read change
	
	echo '' 
	case "$change" in
	#LOGIN
	1) 	
		if [ ${flags[1]} -eq 0 ]
		then
			./login.sh
			flags[1]=1
			echo -e "\e[91m Login: done with your existing user.\e[0m"
			chmod -w user.conf
			chmod -w security_rules.conf
			chmod -w keystonerc_admin					
		else
			echo -e "\e[91m U got actually logged user.\e[0m"
		fi
		;;
	#CREATE NETWORK
	2)
		if [ ${flags[1]} -eq 0 ]
		then
			echo -e "\e[91m Cannot create network. Login first.\e[0m"	
		elif [ ${flags[2]} -eq 1 ]
		then
			echo -e "\e[91m Network already exist. Destroy first.\e[0m"
		else
			./create_network.sh
			./test_vm.sh
			flags[2]=1
			echo -e "\e[91m Create\e[0m"
			chmod -w network.conf
			chmod -w VM.conf 
		fi
		;;
	#DELETE NETWORK
	3)	
		if [ ${flags[2]} -eq 1 ]
		then
			./destroy_network.sh
			echo -e "\e[91m Destroyed\e[0m"
			flags[2]=0
			chmod +w network.conf
			chmod +w VM.conf
		else
			echo -e "\e[91m Nothing to destroy.If u want to destroy create your topology.\e[0m"
		fi
		;;
	#LOGOFF
	4)	
		if [ ${flags[1]} -eq 0 ]
		then
			echo -e "\e[91m Cannot logoff. Login first.\e[0m"
		elif [ ${flags[2]} -eq 1 ]
		then
			echo -e "\e[91m Cannot logoff. Destroy existing network first.\e[0m"
		elif [ ${flags[1]} -eq 1 ]
		then
			./logoff.sh
			flags[1]=0
			echo -e "\e[91m Logoff succesfully.\e[0m"
			chmod +w user.conf
			chmod +w security_rules.conf
			chmod +w keystonerc_admin
		fi
		;;
	#TEST
	5)	
		if [ ${flags[2]} -eq 1 ] 
		then
			./test_network.sh
			echo -e "\e[91m Tested.\e[0m"
		else
			echo -e "\e[91m Nothing to test. Create your network first.\e[0m"
		fi
		;;
	#EXIT
	6)
		if [ ${flags[1]} -eq 0 ]
		then
			flags[0]=0
			echo -e "\e[91m U maked safe exit ;).\e[0m"
		else
			echo -e "\e[91m U cant end program. Logoff first.\e[0m"
		fi
		;;
	*)
		echo -e "\e[91m Signal not found.\e[0m"
		;;
	esac
done
